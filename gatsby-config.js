const { feed } = require("./rss-feed");

module.exports = {
  siteMetadata: {
    title: "Sean Clayton",
    description: "The Personal Website of Sean Clayton",
    siteUrl: "https://www.seanclayton.me",
    links: {
      homepage: "https://seanclayton.me",
      twitter: {
        site_name: "Twitter",
        username: "seanaverage",
        url: "https://twitter.com/seanaverage",
      },
      gitlab: {
        site_name: "GitLab",
        username: "seanclayton",
        url: "https://gitlab.com/seanclayton",
      },
      github: {
        site_name: "GitHub",
        username: "sean-clayton",
        url: "https://github.com/sean-clayton",
      },
      reddit: {
        site_name: "Reddit",
        username: "droctagonapus",
        url: "https://reddit.com/u/droctagonapus",
      },
    },
  },
  plugins: [
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        name: "Sean Clayton",
        short_name: "Sean Clayton",
        start_url: "/",
        background_color: "#fdf6e3",
        theme_color: "#fdf6e3",
        display: "minimal-ui",
        icons: [
          {
            src: `/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ],
      },
    },
    "gatsby-plugin-offline",
    "gatsby-transformer-sharp",
    "gatsby-plugin-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        path: `${__dirname}/src/images`,
        name: "img",
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        path: `${__dirname}/src/_posts`,
        name: "markdown-pages",
      },
    },
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          {
            resolve: "gatsby-remark-prismjs",
            options: {
              classPrefix: "language-",
            },
          },
        ],
      },
    },
    "gatsby-plugin-react-helmet",
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
        {
          site {
            siteMetadata {
              title
              description
              siteUrl
              site_url: siteUrl
            }
          }
        }
      `,
        feeds: [feed],
      },
    },
    {
      resolve: "gatsby-plugin-netlify",
      options: {},
    },
    "gatsby-plugin-emotion",
  ],
};
