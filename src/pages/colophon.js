import React from "react";
import Helmet from "react-helmet";
import PageWrapper from "../components/PageWrapper";
import Prose from "../components/Prose";
import ProseHeader from "../components/ProseHeader";

export default () => (
  <PageWrapper>
    <Helmet
      title="Colophon"
      meta={[
        {
          name: "description",
          content: "A little about how I built my website.",
        },
      ]}
    />
    <ProseHeader>
      <h1>Colophon</h1>
    </ProseHeader>
    <Prose>
      <p>
        This site was made to house my writings and other tidbits somewhere on
        the Internet. It was first born sometime in 2017 and has been
        continuously updated ever since.
      </p>

      <h3>Source Code</h3>
      <p>
        <a href="https://gitlab.com/seanclayton/personal-website">
          https://gitlab.com/seanclayton/personal-website
        </a>
      </p>

      <h3>Software Used</h3>
      <p>
        <a href="https://code.visualstudio.com/">Visual Studio Code</a>
      </p>
      <p>
        <a href="https://www.gatsbyjs.org/">Gatsby</a>
      </p>
      <p>
        <a href="https://www.apple.com/macos/mojave">macOS Mojave</a>
      </p>
    </Prose>
  </PageWrapper>
);
