import React from "react";
import styled, { injectGlobal } from "react-emotion";
import Link from "gatsby-link";
import Image from "gatsby-image";
import PageWrapper from "../components/PageWrapper";

const FourOhFour = styled.div`
  margin-top: 1em;
  display: grid;
  grid-template-areas: "section";

  .gatsby-image-outer-wrapper {
    grid-area: section;
  }
`;

const Lionel = styled(Image)`
  filter: brightness(175%);
  width: 100%;
  object-fit: fill;
`;

const TextOverlay = styled.div`
  display: grid;
  grid-template-areas:
    "top"
    "."
    "bottom";
  grid-template-rows: min-content 1fr min-content;
  grid-area: section;
  z-index: 1;
  background-image: linear-gradient(
    to bottom,
    black 0%,
    transparent 15%,
    transparent 85%,
    white 100%
  );
`;

const Text = styled.p`
  margin: 0.25em 0;
  font-size: 7vw;
  font-weight: bold;

  @media screen and (max-width: 640px) {
    font-size: 2.5em;
  }

  @media screen and (min-width: 900px) {
    font-size: 3.5em;
  }
`;

const TopText = styled(Text)`
  color: white;
  grid-area: top;
  text-align: center;
`;

const BottomText = styled(Text)`
  grid-area: bottom;
  text-align: center;
`;

const Words = styled.div`
  margin-top: 1em;
  font-size: 1.5em;
  text-align: center;
`;

const NotFoundPage = ({ data }) => (
  <PageWrapper>
    <FourOhFour>
      <TextOverlay>
        <TopText>Hello...</TopText>
        <BottomText>Is it me you're looking for?</BottomText>
      </TextOverlay>
      <Lionel
        alt="Lionel Richie and that slick mustache of his."
        sizes={data.lionel.sizes}
      />
    </FourOhFour>
    <Words>
      <p>
        Probably not. <Link to="/">Head 'wards safety you beautiful soul.</Link>
      </p>
    </Words>
    <style>
      {`
        :root {
          background-color: white;
        }
      `}
    </style>
  </PageWrapper>
);

export const pageQuery = graphql`
  query LionelImageQuery {
    lionel: imageSharp(id: { regex: "/lionel-richie.jpg/" }) {
      sizes(maxWidth: 864) {
        base64
        aspectRatio
        src
        srcSet
        sizes
      }
    }
  }
`;

export default NotFoundPage;
