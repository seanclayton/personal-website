import React from "react";
import BlogPostList from "../components/BlogPostList";

const IndexPage = ({ data }) => (
  <BlogPostList posts={data.allMarkdownRemark.edges} />
);

export const pageQuery = graphql`
  query IndexQuery {
    allMarkdownRemark(
      filter: { frontmatter: { publish_date: { ne: null } } }
      sort: { fields: [frontmatter___publish_date], order: DESC }
    ) {
      edges {
        node {
          timeToRead
          wordCount {
            paragraphs
          }
          frontmatter {
            title
          }
          fields {
            slug
          }
        }
      }
    }
  }
`;

export default IndexPage;
