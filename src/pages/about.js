import React from "react";
import styled from "react-emotion";
import Image from "gatsby-image";
import Helmet from "react-helmet";
import PageWrapper from "../components/PageWrapper";
import Prose from "../components/Prose";
import ProseHeader from "../components/ProseHeader";

const ImageWrapper = styled.div`
  mix-blend-mode: multiply;
`;

const Img = styled(Image)`
  filter: grayscale(100%) brightness(150%);
`;

export default ({ data }) => (
  <PageWrapper>
    <Helmet
      title="About"
      meta={[{ name: "description", content: "About me, Sean Clayton." }]}
    />
    <ProseHeader>
      <h1>Howdy!</h1>
    </ProseHeader>
    <Prose>
      <p>
        I'm glad you've found my website, and I hope you've found what I write
        to be somewhat interesting! I found myself really wanting to write about
        certain topics like programming, music, and a bit of personal things I
        find not too many people openly talking about.
      </p>

      <p>
        My name is Sean Clayton. I was born in Vacaville, California and raised
        in good ol' Bardstown, Kentucky until I was 20, which then I moved to
        Louisville, Kentucky. I've resided in several places since then, but I
        like to think that Louisville, Kentucky is my true home.
      </p>

      <p>
        As far as family goes, I have my mom and dad, a brother older than me by
        three years, and a younger sister I'm older than by less than one year.
        I also have a wonderful pet Beagle/Basset Hound named Roscoe.
      </p>

      <p>
        You can also find me on a lot of places on the internet! Listed below
        are the accounts I have on various internet sites:
      </p>

      <p>
        <ul>
          {Object.keys(data.site.siteMetadata.links).map(site => (
            <li key={site}>
              <a href={data.site.siteMetadata.links[site].url}>
                {data.site.siteMetadata.links[site].site_name}
              </a>
            </li>
          ))}
        </ul>
      </p>

      <ImageWrapper>
        <Img alt="Roscoe, my pet dog." sizes={data.roscoe.sizes} />
      </ImageWrapper>
    </Prose>
  </PageWrapper>
);

export const pageQuery = graphql`
  query AboutPageQuery {
    site {
      siteMetadata {
        links {
          twitter {
            site_name
            url
          }
          github {
            site_name
            url
          }
          gitlab {
            site_name
            url
          }
          reddit {
            site_name
            url
          }
        }
      }
    }
    roscoe: imageSharp(id: { regex: "/roscoe.jpg/" }) {
      sizes(maxWidth: 864) {
        base64
        aspectRatio
        src
        srcSet
        sizes
      }
    }
  }
`;
