import React from "react";
import PageWrapper from "../components/PageWrapper";

export default () => (
  <PageWrapper>
    <h1>Resume!</h1>
  </PageWrapper>
);
