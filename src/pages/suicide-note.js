import React from "react";
import remark from "remark";
import remarkReact from "remark-react";
import PageWrapper from "../components/PageWrapper";
import Prose from "../components/Prose";
import ProseHeader from "../components/ProseHeader";
import Markdown from "../components/Markdown";

const markdown = `
### Foreword

Do not be alarmed, this is a suicide note I wrote _many_ years ago.
It has been unaltered, save for one section where I wrote down personal financial information.
I wrote it right before a person noticed I was trying to attempt suicide and called the police.

I have never told anyone about this incident before.
The officers who picked me up never told anyone other than the hospital they dropped me off at.
The people in the hospital who nearly put me in a rehabilitation center never told anyone.
Not a single family member or friend knows I went through this ordeal.
It was a very dark time in my life and was something I was pretty ashamed of—I'm not anymore, though.

I'm putting it on the internet as a reminder for myself and also to make it available for anyone who wishes to see some of the struggles I have thought and experienced. I no longer possess these views of myself or my life.

- - -

I want to die

Please kill me God.
Please end my life

I want to die

Please end the pain I live in every day.
Please end my life

I want to die

Please let me sacrifice myself for the happiness of everyone else
Please end my life

I want to die

Please let everyone be happy
Please end the evil that encapsulates everyone
Please end my life

I want to die

Please take my emptiness and make everyone else feel like they're complete
Please end my life

I want to die

Please let me die God I want my life to be meaningful I want to sacrifice every ounce of happiness I've ever felt and when I die I want my happiness to to go to every one else. It's unfair that I've felt the best possible thing on earth when so many others struggle. I just want to make everyone be happy and I wish that if I killed myself that I released everything I had inside me. I want everything in me to be used by others. I want to die and give so much. I hate hate hate SEAN. Please something kill me. Please kill me. Please kill me. Please kill me. Please end me. I'm never happy anymore. I'm never loved anymore.

I feel like my life is never getting to where I want it to be. I feel like I never will be loved any more. I feel like I'm just a shell who just exists to make everyone else happy. Everyone says "you have to learn to love yourself" well here I am, giving myself all the love by being completely destroyed by a car moving at 55mph. That's the love I have for myself.

I thought about having my mental illness exposed to everyone, and I realized that probably no one will ever care  truly. I've always loved everyone with my full heart. I always have, and I always will. 

I do drugs to forget that I once had all the love I could ever want. I'll never get there again. Every girl I like leaves me, every time I admitted I have a mental illness they leave me. Either I lie and say I'm okay or tell the truth and be alone. Neither are better than the other. I see a road next to me. I want to stand in front of it and be hit. If I do this God, please don't harm the driver or passengers. Please take money out of my account to pay for the damages.

CC #: **\`REDACTED\`**  
Security Code: **\`REDACTED\`**  
Name on Card: **\`REDACTED\`**  
Expiration: **\`REDACTED\`**

Please charge me for everything I caused. I just want to cover all my bases and leave no one harmed/upset. I want to take evey single pill I can think of, but a car seems like the best way of healing the Earth and freeing me from it. I'm sorry God, I'm not strong enough. I am weak. I am dead. I need love so bad.
`;

export default () => (
  <PageWrapper>
    <ProseHeader>
      <h1>I want to die.</h1>
      <time>October 18, 2014 2:41AM</time>
    </ProseHeader>
    <Prose>
      <Markdown markdown={markdown} />
    </Prose>
  </PageWrapper>
);
