import React from "react";
import Helmet from "react-helmet";
import BlogPost from "../components/BlogPost";

export default function Template({ data }) {
  const { markdownRemark } = data;
  const { frontmatter, htmlAst } = markdownRemark;
  return (
    <React.Fragment>
      <Helmet>
        <title>{frontmatter.title}</title>
      </Helmet>
      <BlogPost frontmatter={frontmatter} htmlAst={htmlAst} />
    </React.Fragment>
  );
}

export const query = graphql`
  query SpecificBlogPost($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      htmlAst
      frontmatter {
        title
        publish_date(formatString: "MMMM Do, YYYY")
      }
    }
  }
`;
