---
title: Advanced Permissions with Phoenix
---

I've been working on a side-project over the past year to accomplish a few goals:

1.  Create a production-ready Elixir + Phoenix application with a GraphQL API.
1.  Potentially create a positive cash-flow business and learn everything I can about that experience if I get to that point.

Point #2 aside, the surest thing I can do is make sure I really learn #1.
One thing I noticed with my project is I hade some interesting data models.

- A user creates an organization
- Organizations can be public or private (Only members can do anything—including viewing anything)
- Organizations are really just containers for:
  - A forum, which contain:
    - Categories, which contain:
      - Threads, which contain:
        - Thread replies
  - A calendar, which contain:
    - Calendar events
  - A blog, which contain:
    - Blog posts
  - A wiki, which contain:
    - Wiki pages
- Users can join organizations
- Organizations can create roles and assign users to these roles
- Every "model" in this idea can have permissions:
  - Organization-wide permissions
  - Forum/Calendar/etc-wide permissions
  - Permissions for each forum category (maybe some roles can access a category, while others can't)
- Etc, etc—It's a very advanced permission model.

Basic global role-based permissions are easy, but when organization owners can create roles that can do or not do certain things, it's a bit weird to wrap my head around it.
Also, organizations must have a base role for public people (people who are not members, but try to do things) and members (this role applies to every single member).

<!-- Here's how I did it... -->

<!-- Here are some caveats to that method... -->

<!-- What I've learned and how can this be better for my use-case? -->

<!-- Conclusion -->
