import styled from "react-emotion";

const ProseHeader = styled.header`
  padding: 2em 0;

  h1 {
    font-size: 3em;
    font-weight: 400;
    line-height: 1.5;
  }

  @media screen and (max-width: 768px) {
    h1 {
      font-size: 2.5em;
    }
  }
`;

export default ProseHeader;
