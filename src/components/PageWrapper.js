import React from "react";
import styled from "react-emotion";
import { wrapper } from "../mixins";

const PageWrapper = styled.main`
  ${wrapper};

  > * {
    grid-column: main;
  }
`;

export default PageWrapper;
