import React from "react";
import Link from "gatsby-link";
import styled from "react-emotion";
import { wrapper } from "../mixins";
import MainNav from "./MainNav";
import MainNavLink from "./MainNavLink";

const Header = styled.header`
  ${wrapper};
  background-color: black;
  color: white;
  padding: 3em 0 0 0;

  > * {
    grid-column: main;
  }

  a:not(${MainNavLink}) {
    color: inherit;
    text-decoration: none;
  }

  h1 {
    font-weight: 400;
    margin: 0;
  }

  p {
    font-weight: 300;
  }
`;

export default ({ title, description, location }) => {
  return (
    <Header>
      <h1>
        <Link to="/">{title}</Link>
      </h1>
      <p>{description}</p>
      <MainNav location={location} />
    </Header>
  );
};
