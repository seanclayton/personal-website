import React from "react";
import Link from "gatsby-link";
import styled from "react-emotion";
import { setDisplayName } from "recompose";

const BlogPostListItem = styled(Link)`
  font-size: 1.5em;
  color: rgba(0, 0, 0, 0.8);
  display: block;
  padding: 1em;
  text-decoration: none;
  transition: 0.1s ease all;
  box-shadow: inset 3px 0 0 0 rgba(0, 0, 0, 0.1);
  margin: 0 0 0.5em 0;

  &:hover {
    box-shadow: inset 3px 0 0 0 black;
    background-color: black;
    color: white;
  }

  @media screen and (max-width: 768px) {
    font-size: 1em;
  }
`;

const Info = styled.p`
  margin: 0;
`;

const PostTitle = styled.h3`
  margin: 0 0 0.5em 0;
  font-size: 1.33em;
  font-weight: 400;
`;

const Excerpt = styled.p``;

export default setDisplayName("BlogPostListItem")(({ post }) => (
  <BlogPostListItem to={"post".concat(post.fields.slug)}>
    <PostTitle>{post.frontmatter.title}</PostTitle>
    <Info>
      About {post.timeToRead} {post.timeToRead === 1 ? "minute" : "minutes"} to
      read
    </Info>
  </BlogPostListItem>
));
