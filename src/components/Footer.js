import React from "react";
import styled from "react-emotion";
import MainNavLink from "../components/MainNavLink";
import MainNav from "../components/MainNav";
import { wrapper } from "../mixins";

const Footer = styled.footer`
  ${wrapper};
  background-color: black;
  color: white;
  padding: 0 0 2em 0;
  margin-top: 2em;

  > * {
    grid-column: main;
  }

  a:not(${MainNavLink}) {
    color: inherit;
  }

  ${MainNavLink} {
    color: auto;
    text-decoration: none;
  }

  nav {
    margin-bottom: 1em;
  }
`;

export default ({ location }) => (
  <Footer>
    <MainNav location={location} />
    <span>
      All writings on this site by Sean Clayton are available under the{" "}
      <a href="https://creativecommons.org/licenses/by-sa/4.0/" rel="noopener">
        CC BY-SA 4.0
      </a>{" "}
      license.
      <br />
      <a href="/license.txt">You can view the license here.</a>
    </span>
  </Footer>
);
