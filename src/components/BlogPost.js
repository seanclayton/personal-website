import React from "react";
import styled, { injectGlobal } from "react-emotion";
import rehypeReact from "rehype-react";
import { lifecycle } from "recompose";
import Prose from "./Prose";
import ProseHeader from "./ProseHeader";
import IframeEmbed from "./IframeEmbed";
import Flatten from "./Flatten";
import { wrapper } from "../mixins";

injectGlobal`
code[class*="language-"],
pre[class*="language-"] {
    color: #393A34;
    direction: ltr;
    text-align: left;
    white-space: pre;
    word-spacing: normal;
    word-break: normal;
    font-size: 0.95em;
    line-height: 1.2em;

    tab-size: 4;

    hyphens: none;
}

pre[class*="language-"]::-moz-selection, pre[class*="language-"] ::-moz-selection,
code[class*="language-"]::-moz-selection, code[class*="language-"] ::-moz-selection {
    background: #b3d4fc;
}

pre[class*="language-"]::selection, pre[class*="language-"] ::selection,
code[class*="language-"]::selection, code[class*="language-"] ::selection {
    background: #b3d4fc;
}

/* Code blocks */
pre[class*="language-"] {
    padding: 1em;
    margin: .5em 0;
    overflow: auto;
    border-top: 1px solid #dddddd;
    border-bottom: 1px solid #dddddd;
    background-color: white;
}

:not(pre) > code[class*="language-"],
pre[class*="language-"] {
}

/* Inline code */
:not(pre) > code[class*="language-"] {
    padding: .2em;
    padding-top: 1px; padding-bottom: 1px;
    background: #f8f8f8;
    border-top: 1px solid #dddddd;
    border-bottom: 1px solid #dddddd;
}

.token.comment,
.token.prolog,
.token.doctype,
.token.cdata {
    color: #999988; font-style: italic;
}

.token.namespace {
    opacity: .7;
}

.token.string,
.token.attr-value {
    color: #e3116c;
}
.token.punctuation,
.token.operator {
    color: #393A34; /* no highlight */
}

.token.entity,
.token.url,
.token.symbol,
.token.number,
.token.boolean,
.token.variable,
.token.constant,
.token.property,
.token.regex,
.token.inserted {
    color: #36acaa;
}

.token.atrule,
.token.keyword,
.token.attr-name,
.language-autohotkey .token.selector {
    color: #00a4db;
}

.token.function,
.token.deleted,
.language-autohotkey .token.tag {
    color: #9a050f;
}

.token.tag,
.token.selector,
.language-autohotkey .token.keyword {
    color: #00009f;
}

.token.important,
.token.function,
.token.bold {
    font-weight: bold;
}

.token.italic {
    font-style: italic;
}
`;

const Article = styled(Prose)`
  width: 100%;
  ${wrapper};

  > * {
    grid-column: main;
  }

  > img,
  .gatsby-highlight,
  blockquote,
  .iframe-wrapper {
    width: 100%;
    max-width: 100%;
    grid-column: full;
    margin-left: 0;
    margin-right: 0;
  }
`;

const PostLink = props => <a {...props} />;

const renderMarkdown = new rehypeReact({
  createElement: React.createElement,
  components: {
    iframe: IframeEmbed,
    a: PostLink,
  },
}).Compiler;

export default ({ frontmatter, htmlAst }) => (
  <Article>
    <ProseHeader>
      <h1>{frontmatter.title}</h1>
      {frontmatter.publish_date ? (
        <time>{frontmatter.publish_date}</time>
      ) : null}
    </ProseHeader>
    <Flatten component="div">{renderMarkdown(htmlAst)}</Flatten>
  </Article>
);
