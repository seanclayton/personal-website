import styled from "react-emotion";
import { wrapper } from "../mixins";

const Prose = styled.article`
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-weight: 400;
    margin: 0.5em 0;
    line-height: 1.5;
  }

  h1 {
    font-size: 3em;
  }
  h2 {
    font-size: 2.75em;
  }
  h3 {
    font-size: 2.5em;
  }
  h4 {
    font-size: 2.25em;
  }
  h5 {
    font-size: 2em;
  }
  h6 {
    font-size: 1.75em;
  }

  @media screen and (max-width: 768px) {
    h1 {
      font-size: 2.5em;
    }
    h2 {
      font-size: 2.25em;
    }
    h3 {
      font-size: 2em;
    }
    h4 {
      font-size: 1.75em;
    }
    h5 {
      font-size: 1.5em;
    }
    h6 {
      font-size: 1.25em;
    }
  }

  img {
    margin: 1em 0;
    max-width: 100%;
  }

  figure {
    margin: 0;
  }

  .iframe-wrapper {
    background-color: black;
    margin: 1em 0;
  }

  blockquote {
    ${wrapper};

    p {
      grid-column: main;
      border-left: 3px solid #839496;
      padding-left: 1em;
      font-size: 0.9em;
      font-style: italic;
    }
  }

  .gatsby-highlight {
    margin: 2em 0;

    pre {
      ${wrapper};
      padding: 2em 0;
      margin: 0;

      code {
        padding: 0;
        border: none;
        background: none;
        margin: 0;
        font-style: unset;
      }

      > * {
        grid-column: main;
        white-space: pre-wrap;
        word-break: break-all;
      }
    }
  }

  p,
  p > code,
  li {
    font-size: 1.1rem;
    line-height: 1.7;
    overflow-wrap: break-word;
  }

  a {
    font-weight: bold;
    color: #0077ff;

    &:visited {
      color: #d33682;
    }

    &:active {
      color: #cb4b16;
    }
  }

  pre,
  code {
    font-size: 1.1rem;
    line-height: 1.7;
    font-family: "Dank Mono", "SF Mono", "Monaco", "Inconsolata", "Fira Mono",
      "Droid Sans Mono", "Source Code Pro", monospace;
  }

  code {
    background: white;
    border: 1px solid #e0e0e0;
    padding: 0.25rem 0.5rem;
    color: black;
    font-style: italic;
  }
`;

export default Prose;
