import React from "react";
import Link from "gatsby-link";
import styled from "react-emotion";
import { setDisplayName } from "recompose";
import BlogPostListItem from "./BlogPostListItem";

const BlogPostList = setDisplayName("BlogPostList")(styled.nav`
  ${({ theme }) => `
    max-width: ${theme.siteWidth};
  `};
  width: 100%;
  display: flex;
  flex-direction: column;
  margin: 2em auto;
  list-style-type: none;
  padding: 0;
`);

export default ({ posts }) => (
  <BlogPostList>
    {posts.map(({ node }) => (
      <BlogPostListItem key={node.fields.slug} post={node} />
    ))}
  </BlogPostList>
);
