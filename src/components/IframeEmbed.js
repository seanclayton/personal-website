import React from "react";
import styled from "react-emotion";

const IframeWrapper = styled.div`
  padding-bottom: 56.25%;
  position: relative;
  height: 0px;
  overflow: hidden;
`;

const Iframe = styled.iframe`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

export default props => {
  return (
    <div className="iframe-wrapper">
      <IframeWrapper>
        <Iframe {...props} />
      </IframeWrapper>
    </div>
  );
};
