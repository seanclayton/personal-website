import React, { Fragment, Children } from "react";

const Flatten = ({ component, children }) => {
  const child = Children.only(children);
  if (child.type === component) {
    const newChildren = Children.toArray(child.props.children);
    return <Fragment>{newChildren}</Fragment>;
  }
  return children;
};

export default Flatten;
