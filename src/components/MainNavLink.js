import styled, { css } from "react-emotion";
import Link from "gatsby-link";

const MainNavLink = styled(Link)`
  padding: 1em;
  color: white;
  display: inline-block;
  background-color: rgba(255, 255, 255, 0.1);
  text-decoration: none;

  &:hover {
    color: white;
    background-color: rgba(255, 255, 255, 0.2);
  }
`;

export const activeStyle = css`
  color: black !important;
  background-color: white !important;
`;

export default MainNavLink;
