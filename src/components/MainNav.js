import React from "react";
import styled from "react-emotion";
import MainNavLink, { activeStyle } from "./MainNavLink";

const MainNav = styled.nav`
  display: grid;
  grid-auto-flow: column;
  grid-gap: 1em;
  grid-auto-columns: minmax(min-content, 1fr);

  ${MainNavLink} {
    text-align: center;
  }

  @media screen and (min-width: 640px) {
    grid-auto-columns: min-content;
  }
`;

export default ({ location: { pathname } }) => (
  <MainNav>
    <MainNavLink activeClassName={activeStyle} exact to="/">
      Home
    </MainNavLink>
    <MainNavLink activeClassName={activeStyle} exact to="/about">
      About
    </MainNavLink>
    <MainNavLink activeClassName={activeStyle} exact to="/colophon">
      Colophon
    </MainNavLink>
  </MainNav>
);
