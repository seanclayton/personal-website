import React from "react";
import remark from "remark";
import remarkReact from "remark-react";
import Flatten from "./Flatten";

export default ({ markdown }) => (
  <Flatten component="div">
    {
      remark()
        .use(remarkReact)
        .processSync(markdown).contents
    }
  </Flatten>
);
