import React from "react";
import Helmet from "react-helmet";
import styled, { injectGlobal } from "react-emotion";
import { ThemeProvider } from "emotion-theming";
import theme from "../theme";
import Header from "../components/Header";
import Footer from "../components/Footer";

injectGlobal`
  * { box-sizing: border-box; }
  *:before, *:after {
    box-sizing: inherit;
  }

  :root {
    font-family: "dm", "SF Mono", "Monaco", "Inconsolata", "Fira Mono", "Droid Sans Mono", "Source Code Pro", monospace;
    background-color: #fcfcfc;
    font-size: 18px;
  }

  html, body, #app {
    min-height: 100vh;
    width: 100%;
    margin: 0;
    padding: 0;
  }
`;

const SiteLayout = styled.div`
  display: grid;
  grid-template-areas:
    "header"
    "content"
    "footer";
  grid-template-rows: min-content auto min-content;
`;

const TemplateWrapper = ({ children, data, location }) => {
  const { description, title } = data.site.siteMetadata;
  return (
    <ThemeProvider theme={theme}>
      <SiteLayout id="app">
        <Helmet
          titleTemplate={`%s - ${title}`}
          defaultTitle={title}
          meta={[
            { name: "description", content: description },
            { charset: "utf-8" },
            { "msapplication-TileColor": "#fdf6e3" },
          ]}
        >
          <html lang="en" />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/apple-touch-icon.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/favicon-16x16.png"
          />
          <link rel="stylesheet" ref="style" href="/css/dank-mono.css" />
        </Helmet>
        <Header location={location} title={title} description={description} />
        {children()}
        <Footer location={location} />
      </SiteLayout>
    </ThemeProvider>
  );
};

export default TemplateWrapper;

export const pageQuery = graphql`
  query LayoutQuery {
    site {
      siteMetadata {
        title
        description
      }
    }
  }
`;
