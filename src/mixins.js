export const wrapper = ({ theme }) => `
  display: grid;
  grid-template-columns:
    [full-start] minmax(1em, 1fr)
    [main-start] minmax(0, ${theme.siteWidth}) [main-end]
    minmax(1em, 1fr) [full-end];
  grid-auto-rows: min-content;
`;
