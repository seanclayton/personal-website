module.exports.feed = {
  serialize: ({ query: { site, allMarkdownRemark } }) => {
    return allMarkdownRemark.edges.map(({ node }) => {
      return Object.assign({}, node.frontmatter, {
        description: node.excerpt,
        url: site.siteMetadata.siteUrl + "/post" + node.fields.slug,
        guid: site.siteMetadata.siteUrl + "/post" + node.fields.slug,
        custom_elements: [{ "content:encoded": node.html }],
      });
    });
  },
  query: `
  {
    allMarkdownRemark(
      filter: { frontmatter: { publish_date: { ne: null } } }
      sort: { fields: [frontmatter___publish_date], order: DESC }
    ) {
      edges {
        node {
          excerpt
          html
          fields { slug }
          frontmatter {
            title
            date: publish_date
          }
        }
      }
    }
  }
`,
  output: "/rss.xml",
};
