const path = require("path");
const { createFilePath } = require("gatsby-source-filesystem");

exports.onCreateNode = ({ node, getNode, boundActionCreators }) => {
  const { createNodeField } = boundActionCreators;

  if (node.internal.type === "MarkdownRemark") {
    const slug = `${createFilePath({
      node,
      getNode,
      basePath: "src/_posts",
    })}`;

    createNodeField({
      node,
      name: "slug",
      value: slug,
    });
  }
};

exports.createPages = async ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators;
  const blogPostTemplate = path.resolve("src/templates/blog-post.js");

  const createBlogPost = ({ node }) => {
    createPage({
      path: "post".concat(node.fields.slug),
      component: blogPostTemplate,
      context: {
        slug: node.fields.slug,
      },
    });
  };

  const result = await graphql(`
    {
      allMarkdownRemark(
        filter: { frontmatter: { publish_date: { ne: null } } }
      ) {
        edges {
          node {
            id
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  if (result.errors) {
    console.error(result.errors);
  }

  result.data.allMarkdownRemark.edges.forEach(createBlogPost);
};
